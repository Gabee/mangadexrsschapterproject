import json, feedparser
from pprint import pprint
from send import send_message

def updateFile(dJson):
    jsonFile = open("document.json", "w+")
    jsonFile.write(json.dumps(dJson))
    jsonFile.close()

def readJson():
    with open('document.json') as f:
        data = json.load(f)
        return data

def parse(url, last_chapter, json_data):
    feed = feedparser.parse(url)

    for item in feed["items"]:
        if checkData(item["title" ], item["summary"], last_chapter):
            subj = item["title"]
            body = item["link"]
            #print("MSG: {}".format(payload))
            print(item["published"])

            # update the chapter
            json_data["last_chapter"] = int(last_chapter) + 1
            print("Chapter has been updated")

            # send message
            send_message(subj, body)

def checkData(titleDescr, summaryDescr, last_chapter):
    # Go to the next chapter
    last_chapter = str(int(last_chapter) + 1)
    if titleDescr.find('Chapter ' + last_chapter)!= -1 and summaryDescr.find('Language: English') != -1:
        print("{}->{}".format(titleDescr.encode('utf-8'), summaryDescr.encode('utf-8')))
        return True

def main():
    with open('document.json') as f:
        data = json.load(f)
    
        for d in data:  
            parse(d["link"], d["last_chapter"], d)
    
        updateFile(data)
    